/*
	functions
		- functions are lines/block of codes that tell our device/application to perfomr certain tasks when called/invoked
		- functions are mostly created to create complicated tasks to fun several lines of code in succession
		- they are also used to prevent repeating lines/blocks of code that perform the same tasks/function

		syntax:
			function functionName(){
				code block(statement)
			}
		>> function keyword
			- used to define a javascript functions

		>> functionName
			- function name. Functions are named to be able to use later in the code.

		>> function block ({})
			- the statements which comprise the body of the function. This is where the code to be executed.
*/

function printName(){
	console.log("My Name is Jungkok. I am an Idol.");
};

// function invocation - it is common to use the term "call a function" instead of "Invoke a function"

printName();

declaredFunction();
//result: err, because it is not yet defined

function declaredFunction(){
	console.log("This is a define function");
};



/*
	function expression
		- a function can also be stored in a variable. This is called a function expression
		- a function expression is an anonymous function assigned to a variable function

		anonymous function - function without a name

*/
let variableFunction = function(){
	console.log("I am from variable function");
};

variableFunction();
/*
	We can also create a function expression of a named function. However, to invoke a function expression, we invoke it by its variable name, not by its function name

	Function expressions are always invoke(called) using the variable name
*/
let functionExpression = function funcName(){
	console.log("Hello from the other side.");
};

functionExpression();

// You can reassign declared function and function expression to new anonymous function

declaredFunction = function(){
	console.log("Updated declared function");
};

declaredFunction();

functionExpression = function(){
	console.log("Updated function expression");
};

functionExpression();

const constantFunction = function(){
	console.log("Initialized with const.");
};

constantFunction();
// reassignment with const function expression is not possible

/*
	Function Scoping
		* Scope is the accessibility (visibility) of variables within our program

			Javascript Variables has 3 types of scope:
				1. Local/block scope
				2. global scope
				3. function scope
*/

{
	let localVar = "Kim Seok-Jin";
	console.log(localVar);
	// local variable will be visible only within a function, where it is defined 
}

let globalVar = "The World's Most Handsome";

console.log(globalVar);
//a global variable has global scope, which means it can be define anywhere in your javascript code


//Function Scope
/*
	Javascript has a function scope: Each function creates a new scope.
	Variables defined inside a function are not accessible (visible) from outside a function

	Variables declared with var, let and const are quite similar when declared inside a function
*/

function showName(){
	var functionVar = "Jungkook";
	const functionConst = "BTS";
	let functionLet = "Kookie";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showName();

//Nested Function
// You can create another function inside a function. This is called a nested function. This nested function, being inside a new function will have variable, name as they are within the same scope/code block

function myNewFunction(){
	let name = "Yor";

	function nestedFunction(){
		let nestedName = "Brando";
		console.log(nestedName);
	};
	nestedFunction();
};

myNewFunction();

//Function and Global Scoped Variables
// Global Scoped Variable
let globalName = "Jean";

function myNewFunction2(){
	let nameInside = "Victoria";

	//Variables declared globally (outside functions) have Global Scope 
	// Global variables can be accessed from anywhere in a Javascript program including from inside a funciton.

	console.log(globalName);
	console.log(nameInside);
};

myNewFunction2();

/*
	alert()
		syntax:
			alert("message");
*/
alert("Hello World");

function showSampleAlert(){
	alert("Hello User");
};

showSampleAlert();

console.log("I will only log in the console when the alert is dismissed");

/*
	prompt()
		syntax:
			promt("<dialog>");
*/

let samplePrompt = prompt('Enter your name: ');
console.log("Hello " + samplePrompt);

let sampleNullPrompt = prompt("Don't input anything ");
console.log(sampleNullPrompt);
// if prompt() is cancelled, the result will be null
// if there is no input in the prompt() the result will be an empty string;

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to Gamer's Guild.");
}; 

printWelcomeMessage();

//Function Naming Convention
	//Function should be definitive of its task. Usually contains a verb

function getCourses(){
	let courses = ["Programming 100","Science 101","Grammar 102","Mathematics 103",];
	console.log(courses);
};

getCourses();

// Avoid generic names to avoid confusion
function get(){
	let name = "Jimin";
	console.log(name);
};

get();

//name function in small caps. Follow camelcase when naming functions

function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000");
};

displayCarInfo();
